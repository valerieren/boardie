import blackjack from "./games/blackjack";
import coinToss from "./games/coinToss";
import prisonersDilemma from "./games/prisonersDilemma";
import uno from "./games/uno";
import rockPaperScissors from "./games/rockPaperScissors";

export default { // TODO give this types
    gameDefinitions: [blackjack, coinToss, prisonersDilemma, uno, rockPaperScissors]
};
