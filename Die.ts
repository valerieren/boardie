import { uintCheck, randomInt } from "./Common";

export default class Die {
    sides: number;

    constructor(sides: number = 6) {
        uintCheck(sides);
        this.sides = sides;
    }

    roll(): number {
        return randomInt(this.sides) + 1;
    }
}