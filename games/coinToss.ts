import GameDefinition from "../GameDefinition";
import Die from "../Die";
import Player from "../Player";
import AllPlayersStep from "../steps/AllPlayersStep";
import SetupCompletedStep from "../steps/SetupCompletedStep";
import RepeaterStep from "../steps/RepeaterStep";
import GlobalStep from "../steps/GlobalStep";

const coinToss = new GameDefinition("Coin toss");
const coin = new Die(2);

const money = (player: Player): number => player.properties.int("money");

coinToss.addStep(new AllPlayersStep("Player setup", async (game, player) => player.properties.newInt("money")));
coinToss.addStep(new SetupCompletedStep());
coinToss.addStep(new RepeaterStep("Game", game => !!game.data.players.find(p => p.alive))
    .addStep(new AllPlayersStep("Player turn", async (game, player) => {
        if (coin.roll() == 1) {
            game.log(`${player.name} is out! They won ${money(player)} dollars`);
            player.alive = false;
        } else {
            game.log(`${player.name} gets a coin!`);
            player.properties.changeInt("money", 1);
        }
    })));

coinToss.addStep(new GlobalStep("Results", async game => {
    const rankings = game.data.players.sort((p1, p2) => money(p1) - money(p2));
    game.log("The final results are: ");
    rankings.forEach(player => game.log(`${player.name}: ${money(player)}`));
    game.log(`The winner is: ${rankings[rankings.length - 1].name}`);
}));

export default coinToss;