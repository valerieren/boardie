import { PerPlayerHandler, NoopHandler } from "./Common";
import Game from "./Game";
import Player from "./Player";
import { v4 as uuid } from "uuid";
import { ChoiceType } from "./ChoiceType";

export default class Choice {
    readonly id = uuid();
    readonly type: ChoiceType;
    text: string;
    effect: PerPlayerHandler;

    constructor(text: string, effect: PerPlayerHandler, type: ChoiceType = ChoiceType.Generic) {
        this.text = text;
        this.effect = effect;
        this.type = type;
    }

    async apply(game: Game, player: Player): Promise<void> {
        await this.effect(game, player);
    }
}