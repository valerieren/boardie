import AbstractStep from "./AbstractStep";
import Game from "../Game";
import { AsyncHandler } from "../Common";

export default class extends AbstractStep<AsyncHandler<Game>> {
    constructor(name: string, action: AsyncHandler<Game>) {
        super(name, action);
    }
};