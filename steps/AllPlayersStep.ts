import AbstractStep from "./AbstractStep";
import { AsyncHandler, PerPlayerHandler, PlayerOrderHandler, uintCheck, DefaultPlayerOrderHandler } from "../Common";
import Game from "../Game";
import Player from "../Player";

export default class extends AbstractStep<AsyncHandler<Game>> {
    constructor(name: string, action: PerPlayerHandler,
         playerOrderHandler: PlayerOrderHandler = DefaultPlayerOrderHandler)  {
        super(name, async (game) => {
            // What if we don't want to start from the "starting player"?
            game.data.currentPlayer = game.data.players[game.data.startingPlayerIndex];
            game.data.currentPlayerIndex = game.data.startingPlayerIndex;
            for(;;) { // Players
                if (game.data.currentPlayer.alive) {
                    game.log(`Current player: ${game.data.currentPlayer.name}`);
                    await action(game, game.data.currentPlayer);
                }

                const nextPlayer = this.getNextPlayer(game, playerOrderHandler);
                if (!nextPlayer) {
                    break;
                }
                game.data.currentPlayer = nextPlayer;
                game.data.currentPlayerIndex = game.data.players.indexOf(nextPlayer); // TODO do game based on player ID
            }
        });
    }

    getNextPlayer(game: Game, playerOrderHandler: PlayerOrderHandler): Player | null {
        const player = playerOrderHandler(game, game.data.currentPlayer, game.data.currentPlayerIndex);
        if (player == null) {
            return null;
        }
        if (player instanceof Player) {
            return player;
        }

        uintCheck(player);
        return game.data.players[player] || null;
    }
};