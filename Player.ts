import { uintCheck, Handler, NoopHandler } from "./Common";
import Card from "./Card";
import Deck from "./Deck";
import Properties from "./Properties";
import { v4 as uuid } from "uuid";
import { Visibility } from "./Visibility";

export default class Player {
    id: string = uuid();
    name: string = "";
    ai: boolean = false;
    alive: boolean = true;
    properties: Properties = new Properties();
    hand: Card[] = [];

    playerStateChangedHandler: Handler<Player> = NoopHandler; // TODO this doesn't handle properties right now, only the hand

    constructor(name: string, ai: boolean = false) {
        this.name = name;
        this.ai = ai;
    }

    discard(card: Card): void {
        const index = this.hand.indexOf(card);
        if (index < 0) {
            throw new Error(`${card.name} is not in ${name}'s hand!`);
        }

        this.hand.splice(index, 1);
        card.discard();
        this.playerStateChangedHandler(this);
    }

    drawAndKeep(deck: Deck): Card {
        const card = deck.randomAndRemove();
        this.hand.push(card);
        this.playerStateChangedHandler(this);
        return card;
    }

    drawAndKeepMany(deck: Deck, count: number): Card[] {
        uintCheck(count);
        const cards = [];
        for (let i = 0; i < count; ++i) {
            cards.push(this.drawAndKeep(deck));
        }

        return cards;
    }

    viewedAs(viewer: Player): Player {
        const self = this == viewer;
        const player = new Player(this.name, this.ai);
        player.id = this.id;
        player.alive = this.alive;
        player.properties = this.properties.clone(); // Visibility?
        player.hand = this.hand
            .filter(c => self ? c.selfVisibility != Visibility.None : c.opponentVisibility != Visibility.None)
            .map(c => self
                ? (c.selfVisibility == Visibility.Full ? c : c.dummy())
                : (c.opponentVisibility == Visibility.Full ? c : c.dummy()));
        return player;
    }
}