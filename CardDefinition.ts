import { PerPlayerHandler } from "./Common";
import Properties from "./Properties";

export default class {

    readonly effect: PerPlayerHandler;
    readonly name: string;
    properties: Properties = new Properties();

    constructor(name: string, effect: PerPlayerHandler) {
        this.name = name;
        this.effect = effect;
    }
}
