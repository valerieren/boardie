import GameDefinition from "../GameDefinition";
import Decision from "../Decision";
import SetupCompletedStep from "../steps/SetupCompletedStep";
import GlobalStep from "../steps/GlobalStep";
import AllPlayersParallelStep from "../steps/AllPlayersParallelStep";
import Choice from "../Choice";

const prisonersDilemma = new GameDefinition("Prisoners' dilemma", 2, 2);
prisonersDilemma.addStep(new SetupCompletedStep());

const SILENT = "silent";  // TODO enum type property?
const BETRAY = "betray";

const decision = new Decision("Stay silent or betray?");
decision.addChoice(new Choice("Stay silent", async (game, player) => {
    player.properties.newString("choice", SILENT);
    player.alive = false;
}));
decision.addChoice(new Choice("Betray", async (game, player) => {
    player.properties.newString("choice", BETRAY);
    player.alive = false;
}));

prisonersDilemma.addStep(new AllPlayersParallelStep("Decision time", async (game, player) => {
    (await game.needDecision(decision, player)).apply(game, player);
}));

prisonersDilemma.addStep(new GlobalStep("Results", async game => {
    const p1 = game.data.players[0];
    const p2 = game.data.players[1];

    if (p1.properties.string("choice") == SILENT) {
        if (p2.properties.string("choice") == SILENT) {
            game.log("You both stayed silent and were sentenced to 1 year1 each!");
        } else {
            game.log(`${p2.name} betrayed ${p1.name}! ${p2.name} goes free and ${p1.name} gets 3 years!`);
        }
    } else {
        if (p2.properties.string("choice") == BETRAY) {
            game.log("You both betrayed each other and were sentenced to 2 years each!");
        } else {
            game.log(`${p1.name} betrayed ${p2.name}! ${p1.name} goes free and ${p2.name} gets 3 years!`);
        }
    }
}));

export default prisonersDilemma;