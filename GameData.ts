import Deck from "./Deck";
import Player from "./Player";
import Properties from "./Properties";
import { v4 as uuid } from "uuid";

export default class GameData {
    id = uuid();
    decks: Map<string, Deck> = new Map();
    players: Player[] = [];
    currentPlayer!: Player; // Will be initialised when the game starts
    currentPlayerIndex: number = -1;
    startingPlayerIndex: number = 0;
    properties: Properties = new Properties();

    viewedAs(player: Player): GameData {
        const data = new GameData();
        data.id = this.id;
        data.decks = new Map(this.decks);
        data.players = this.players.map(p => p.viewedAs(player));
        data.currentPlayer = this.currentPlayer?.viewedAs(player);
        data.currentPlayerIndex = this.currentPlayerIndex;
        data.startingPlayerIndex = this.startingPlayerIndex;
        data.properties = this.properties.clone();
        return data;
    }
}