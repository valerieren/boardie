import Game from "./Game";
import { uintCheck, AsyncHandler } from "./Common";
import DeckDefinition from "./DeckDefinition";
import AbstractStep from "./steps/AbstractStep";

export default class GameDefinition {
    readonly name: string;
    readonly maxPlayers: number;
    readonly minPlayers: number;
    readonly decks: Map<string, DeckDefinition> = new Map();//Deck[] = []; // TODO these are currently shared between game instances
    readonly steps: AbstractStep<AsyncHandler<Game>>[] = [];

    constructor(name: string, minPlayers: number = 1, maxPlayers = Number.MAX_SAFE_INTEGER) {
        uintCheck(minPlayers);
        uintCheck(maxPlayers);
        this.name = name;
        this.minPlayers = minPlayers;
        this.maxPlayers = maxPlayers;
    }

    addDeck(name: string): DeckDefinition {
        if (this.decks.has(name)) {
            throw new Error("A deck with that name already exists");
        }

        const deck = new DeckDefinition(name);
        this.decks.set(name, deck);
        return deck;
    }

    addStep(step: AbstractStep<AsyncHandler<Game>>): void {
        this.steps.push(step);
    }

    createGame(): Game {
        return new Game(this);
    }
}