import AbstractStep from "./AbstractStep";
import { PerPlayerHandler } from "../Common";

export default class extends AbstractStep<PerPlayerHandler> {
    constructor(name: string, action: PerPlayerHandler) {
        super(name, action);
    }
};