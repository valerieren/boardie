import GameDefinition from "./GameDefinition";
import Player from "./Player";
import Decision from "./Decision";
import Choice from "./Choice";
import { DecisionHandler, GameOver, NoopHandler, Handler2, Handler, AsyncHandler } from "./Common";
import { GameState } from "./GameState";
import Deck from "./Deck";
import GameData from "./GameData";

export default class Game {
    readonly definition: GameDefinition;

    data: GameData = new GameData();

    decisionHandler: DecisionHandler = () => { throw new Error("No decision handler specified") };
    logHandler: (msg: string) => void = () => { throw new Error("No log handler specified") };
    setupCompletedHandler: Handler<Game> = NoopHandler;
    playerJoinedHandler: Handler<Game> = NoopHandler;
    playerStateChangedHandler: Handler2<Game, Player> = NoopHandler;

    private _state: GameState = GameState.Created;
    get state(): GameState {
        return this._state;
    }

    constructor(definition: GameDefinition) {
        this.definition = definition;
        this.definition.decks.forEach((deck, name) => this.data.decks.set(name, new Deck(deck)));
    }

    addPlayer(name: string = "", ai: boolean = false): Player {
        if (this.data.players.length == this.definition.maxPlayers) {
            throw new Error(`Game is already at a maximum of ${this.definition.maxPlayers} players`);
        }
        // TODO check if game is running or not? Blackjack adds the AI player after the game starts
        if (!name) {
            name = `Player ${this.data.players.length + 1}`; // First player will become Player 1
        }

        if (ai) {
            name = `[AI] ${name}`;
        }

        const player = new Player(name, ai);
        player.playerStateChangedHandler = (player: Player) => this.playerStateChangedHandler(this, player);
        this.data.players.push(player);
        this.playerJoinedHandler(this);
        return player;
    }

    deck(name: string): Deck {
        if (!this.data.decks.has(name)) {
            throw new Error("No deck with that name exists");
        }

        return this.data.decks.get(name)!;
    }

    log(message: string): void {
        this.logHandler(message);
    }

    async needDecision(decision: Decision, target: Player): Promise<Choice> {
        if (target.ai) {
            return decision.aiHandler(decision, target);
        }
        return this.decisionHandler(decision, target);
    }

    async play(): Promise<void> {
        if (this._state != GameState.Created) {
            throw new Error(`Game was already started once`);
        }
        if (this.data.players.length < this.definition.minPlayers) {
            throw new Error(`Cannot start a game with less than ${this.definition.minPlayers} players`);
        }

        this._state = GameState.Started;

        try {
            // We cannot wait inside .forEach, so gotta go old-fashioned
            for (let i = 0; i < this.definition.steps.length; ++i) {
                this.log(`Starting step [${this.definition.steps[i].name}]`);
                await this.definition.steps[i].execute(this);
            }
        } catch (error) {
            if (!(error instanceof GameOver)) {
                throw error; // We only care about GameOvers here, everything else is unhandled
            }
            this.log("Game over!");
        }
    }
}