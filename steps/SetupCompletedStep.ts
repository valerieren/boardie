import AbstractStep from "./AbstractStep";
import { AsyncHandler } from "../Common";
import Game from "../Game";

export default class extends AbstractStep<AsyncHandler<Game>> {
    constructor() {
        super("Setup completed", async (game) => await game.setupCompletedHandler(game));
    }
};