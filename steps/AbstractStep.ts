export default abstract class AbstractStep<T> {
    readonly name: string;
    readonly execute: T;

    constructor(name: string, executor: T) {
        this.name = name;
        this.execute = executor;
    }
};