import AbstractStep from "./AbstractStep";
import { Handler, AsyncHandler } from "../Common";
import Game from "../Game";

export default class RepeaterStep extends AbstractStep<AsyncHandler<Game>> {
    steps: AbstractStep<AsyncHandler<Game>>[] = [];

    constructor(name: string, condition: Handler<Game, boolean>) {
        super(name, async (game) => {
            while (condition(game)) {
                for (let i = 0; i < this.steps.length; ++i) {
                    await this.steps[0].execute(game);
                }
            }
        });
    }

    addStep(step: AbstractStep<AsyncHandler<Game>>): RepeaterStep {
        this.steps.push(step);
        return this;
    }
};